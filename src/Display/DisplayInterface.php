<?php

namespace App\Display;

use App\PlayedCard;
use App\Score;
use App\ScoreList;

interface DisplayInterface
{
    public function displayCardPlayed(PlayedCard $playedCard): void;
    public function displayRoundWinner(PlayedCard $playedCard): void;
    public function displayScore(ScoreList $scoreList): void;
    /**
     * @param Score[] $winnerList
     */
    public function displayWinnerList(array $winnerList): void;
    public function displayError(string $errorMessage): void;
}
