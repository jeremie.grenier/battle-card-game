<?php

namespace App\Display;

use App\PlayedCard;
use App\ScoreList;

class DebugConsoleDisplay implements DisplayInterface
{
    public function displayWinnerList(array $winnerList): void
    {
        echo 1 !== count($winnerList) ? "Winners: \n" : 'Winner: ';

        foreach ($winnerList as $winner) {
            echo sprintf(
                "%s with %s score\n",
                $winner->getPlayer()->getIdentifier(),
                $winner->getValue()
            );
        }
    }

    public function displayScore(ScoreList $scoreList): void
    {
        echo "Score\n\n\n";

        foreach ($scoreList->getScoreList() as $score) {
            echo sprintf(
                "Player %s: %s\n\n",
                $score->getPlayer()->getIdentifier(),
                $score->getValue()
            );
        }
    }

    public function displayCardPlayed(PlayedCard $playedCard): void
    {
        echo sprintf(
            "Player %s play card %s\n",
            $playedCard->getPlayer()->getIdentifier(),
            $playedCard->getCardValue()
        );
    }

    public function displayRoundWinner(PlayedCard $playedCard): void
    {
        echo sprintf(
            "Card max is %s of player %s\n",
            $playedCard->getCardValue(),
            $playedCard->getPlayer()->getIdentifier(),
        );
    }

    public function displayError(string $errorMessage): void
    {
        echo sprintf(
            "An error occurred\nDetails: %s\n",
            $errorMessage
        );
    }
}
