<?php

namespace App\Display;

use App\PlayedCard;
use App\ScoreList;

class ConsoleDisplay implements DisplayInterface
{
    public function displayWinnerList(array $winnerList): void
    {
        echo 1 !== count($winnerList) ? "Winners: \n" : 'Winner: ';

        foreach ($winnerList as $winner) {
            echo sprintf(
                "%s with %s score\n",
                $winner->getPlayer()->getIdentifier(),
                $winner->getValue()
            );
        }
    }

    public function displayScore(ScoreList $scoreList): void
    {
        echo "Score\n\n\n";

        foreach ($scoreList->getScoreList() as $score) {
            echo sprintf(
                "Player %s: %s\n\n",
                $score->getPlayer()->getIdentifier(),
                $score->getValue()
            );
        }
    }

    public function displayCardPlayed(PlayedCard $playedCard): void
    {
        // do nothing
    }

    public function displayRoundWinner(PlayedCard $playedCard): void
    {
        // do nothing
    }

    public function displayError(string $errorMessage): void
    {
        echo 'An error occurred';
    }
}
