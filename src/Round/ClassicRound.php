<?php

namespace App\Round;

use App\PlayedCard;

class ClassicRound implements RoundInterface
{
    /**
     * @var PlayedCard[]
     */
    private array $cardPlayedList;

    /**
     * Add a new card to the round
     */
    public function addPlayedCard(PlayedCard $playedCard): void
    {
        $this->cardPlayedList[] = $playedCard;
    }

    /**
     * Get the highest
     */
    public function getWinnerCardPlayed(): PlayedCard
    {
        uasort($this->cardPlayedList, static function (PlayedCard $cardA, PlayedCard $cardB) {
            if ($cardA->getCardValue() === $cardB->getCardValue()) {
                return 0;
            }
            return ($cardA->getCardValue() > $cardB->getCardValue()) ? -1 : 1;
        });

        return reset($this->cardPlayedList);
    }
}
