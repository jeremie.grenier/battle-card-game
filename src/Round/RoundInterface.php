<?php

namespace App\Round;

use App\PlayedCard;

interface RoundInterface
{
    public function addPlayedCard(PlayedCard $playedCard): void;
    public function getWinnerCardPlayed(): PlayedCard;
}
