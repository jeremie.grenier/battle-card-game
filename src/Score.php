<?php

namespace App;

class Score
{
    private Player $player;

    private int $value;

    /**
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
        $this->value = 0;
    }

    public function incrementValue(): void
    {
        $this->value++;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getPlayer(): Player
    {
        return $this->player;
    }
}
