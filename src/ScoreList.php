<?php

namespace App;

use App\Exception\PlayerScoreNotFoundException;

class ScoreList
{
    /** @var Score[] */
    private array $scoreList;

    /**
     * @param Score[] $scoreList
     */
    public function __construct(array $scoreList)
    {
        $this->scoreList = $scoreList;
    }

    /**
     * @return Score[]
     */
    public function getWinnerList(): array
    {
        $maxScoreValue = 0;
        $winnerList = [];

        foreach ($this->scoreList as $score) {
            if ($score->getValue() > $maxScoreValue) {
                $maxScoreValue = $score->getValue();
                $winnerList = [$score];
            } elseif ($score->getValue() === $maxScoreValue) {
                $winnerList[] = $score;
            }
        }

        return $winnerList;
    }

    /**
     * Increment score of player
     *
     * @throws \Exception If score of player is not found
     */
    public function incrementScoreOfPlayer(Player $player): void
    {
        $this->getScoreOfPlayer($player)->incrementValue();
    }

    /**
     * Get the score of a player
     *
     * @throws PlayerScoreNotFoundException thrown when score not found
     */
    private function getScoreOfPlayer(Player $player): Score
    {
        foreach ($this->scoreList as $score) {
            if ($score->getPlayer() === $player) {
                return $score;
            }
        }

        throw new PlayerScoreNotFoundException('No score found for player');
    }

    /**
     * @return Score[]
     */
    public function getScoreList(): array
    {
        return $this->scoreList;
    }
}
