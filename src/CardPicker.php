<?php

namespace App;

class CardPicker
{
    /**
     * @var Card[] List of cards
     */
    private array $cardList;

    /**
     * @param Card[] $cardList
     */
    public function __construct(array $cardList)
    {
        $this->cardList = $cardList;
    }

    public function shuffle(): void
    {
        shuffle($this->cardList);
    }

    /**
     * Deal cards to all players
     *
     * @param Player[] $playerList
     */
    public function deal(array $playerList): void
    {
        do {
            foreach ($playerList as $player) {
                if (!empty($this->cardList)) {
                    $player->addCard(array_shift($this->cardList));
                }
            }
        } while (!empty($this->cardList));
    }
}
