<?php

namespace App;

use App\Display\DisplayInterface;
use App\Exception\PlayerScoreNotFoundException;
use App\Round\ClassicRound;
use App\Round\RoundInterface;

class Game
{
    /**
     * @var CardPicker Card picker of the game
     */
    private CardPicker $cardPicker;

    /**
     * @var Player[] List of player of games
     */
    private array $playerList;

    /**
     * @var DisplayInterface
     */
    private DisplayInterface $display;

    /**
     * @param CardPicker $cardPicker
     * @param Player[] $playerList
     */
    public function __construct(CardPicker $cardPicker, array $playerList, DisplayInterface $display)
    {
        $this->cardPicker = $cardPicker;
        $this->playerList = $playerList;
        $this->display = $display;
    }

    public function play(): void
    {
        // shuffle card
        $this->cardPicker->shuffle();

        // deal cards to players
        $this->cardPicker->deal($this->playerList);

        // init scores
        $scores = [];
        foreach ($this->playerList as $player) {
            $scores[] = new Score($player);
        }

        $scoreList = new ScoreList($scores);

        // while we can play
        while ($this->allPlayersHasCard()) {
            $this->playRound($scoreList);
        }

        $winnerList = $scoreList->getWinnerList();
        $this->display->displayScore($scoreList);
        $this->display->displayWinnerList($winnerList);
    }

    /**
     * Check if game can continue
     *
     * @return bool
     */
    private function allPlayersHasCard(): bool
    {
        foreach ($this->playerList as $player) {
            if (!$player->hasCard()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Play the round
     */
    private function playRound(ScoreList $scoreList): void
    {
        $round = new ClassicRound();

        $this->playCard($round);

        $maxCard = $round->getWinnerCardPlayed();
        $this->display->displayRoundWinner($maxCard);

        try {
            $scoreList->incrementScoreOfPlayer($maxCard->getPlayer());
        } catch (PlayerScoreNotFoundException $e) {
            $this->display->displayError($e->getMessage());
            exit(1);
        }
    }

    /**
     * Ask all player to play their card
     */
    private function playCard(RoundInterface $round): void
    {
        foreach ($this->playerList as $player) {
            $card = $player->card();

            $playedCard = new PlayedCard($card, $player);
            $round->addPlayedCard($playedCard);

            $this->display->displayCardPlayed($playedCard);
        }
    }
}
