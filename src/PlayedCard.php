<?php

namespace App;

class PlayedCard
{
    private Card $card;
    private Player $player;

    /**
     * @param Card $card
     * @param Player $player
     */
    public function __construct(Card $card, Player $player)
    {
        $this->card = $card;
        $this->player = $player;
    }

    public function getCardValue(): int
    {
        return $this->card->getValue();
    }

    /**
     * @return Player
     */
    public function getPlayer(): Player
    {
        return $this->player;
    }
}
