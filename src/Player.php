<?php

namespace App;

class Player
{
    /**
     * @var Card[] List of cards
     */
    private array $cardList;

    private int $identifier;

    /**
     * @param int $identifier
     */
    public function __construct(int $identifier)
    {
        $this->identifier = $identifier;
        $this->cardList = [];
    }

    /**
     * Add a card in the user hand
     */
    public function addCard(Card $card): void
    {
        $this->cardList[] = $card;
    }

    /**
     * Check if user has at least one card to play
     */
    public function hasCard(): bool
    {
        return !empty($this->cardList);
    }

    public function card(): Card
    {
        return array_shift($this->cardList);
    }

    /**
     * @return int
     */
    public function getIdentifier(): int
    {
        return $this->identifier;
    }
}
