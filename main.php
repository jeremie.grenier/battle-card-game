<?php

use App\Card;
use App\CardPicker;
use App\Display\ConsoleDisplay;
use App\Display\DebugConsoleDisplay;
use App\Game;
use App\Player;

require_once 'autoload.php';

// create display from env
$display = "true" === getenv('DEBUG') ? new DebugConsoleDisplay() : new ConsoleDisplay();

// create card list
$cards=[];
for ($i=1; $i<=52; $i++) {
    $cards[] = new Card($i);
}

// create cardPicker
$cardPicker = new CardPicker($cards);

// create Player
$player = new Player(1);
$player2 = new Player(2);

// init game
$game = new Game($cardPicker, [$player, $player2], $display);

// play
$game->play();
