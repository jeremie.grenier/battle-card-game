.PHONY: docker-build docker-run docker-run-debug docker-sh

## Build Docker images
docker-build:
	DOCKER_BUILDKIT=1 docker build -t battle-game-php:latest . --target php

## Run image
docker-run:
	docker run -it battle-game-php:latest php main.php

## Run image with debug mode on
docker-run-debug:
	docker run -it -e DEBUG=true battle-game-php:latest php main.php

## Connect to container and mount volume
docker-sh:
	docker run -it -v $(shell pwd):/var/www battle-game-php:latest sh